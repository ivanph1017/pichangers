package pe.edu.ulima.pichangers.models.dtos;

import java.util.Map;

/**
 * Created by Ivan on 27/11/2016.
 */

public class Team {
    private String name;
    private int victories;
    private int losses;
    private String urlImage;
    private Map<String, Player> stringPlayerMap;

    public Team() {
    }

    public Team(int losses, String name, String urlImage, Map<String, Player> stringPlayerMap, int victories) {
        this.losses = losses;
        this.name = name;
        this.urlImage = urlImage;
        this.stringPlayerMap = stringPlayerMap;
        this.victories = victories;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Player> getStringPlayerMap() {
        return stringPlayerMap;
    }

    public void setStringPlayerMap(Map<String, Player> stringPlayerMap) {
        this.stringPlayerMap = stringPlayerMap;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }
}
