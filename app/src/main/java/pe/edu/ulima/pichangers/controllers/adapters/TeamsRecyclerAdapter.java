package pe.edu.ulima.pichangers.controllers.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.controllers.activities.PlayersActivity;
import pe.edu.ulima.pichangers.controllers.activities.TeamInfoActivity;
import pe.edu.ulima.pichangers.models.dtos.Team;

/**
 * Created by Ivan on 27/11/2016.
 */

public class TeamsRecyclerAdapter extends RecyclerView.Adapter<TeamsRecyclerAdapter.ViewHolder>{
    private List<Team> teamList;
    private LayoutInflater mInflater;
    private Context mContext;
    private Intent mIntent;

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected CardView cardView;
        protected ImageView ivTeam;
        protected ImageView iconListPlayers;
        protected ImageView iconPlusPlayer;
        protected ImageView iconShareTeam;
        protected TextView tvTeamName;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            ivTeam = (ImageView) itemView.findViewById(R.id.ivTeam);
            iconListPlayers = (ImageView) itemView.findViewById(R.id.iconListPlayers);
            iconPlusPlayer = (ImageView) itemView.findViewById(R.id.iconPlusPlayer);
            iconShareTeam = (ImageView) itemView.findViewById(R.id.iconShareTeam);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
        }
    }

    public TeamsRecyclerAdapter(Context mContext, Intent mIntent) {
        this.mInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
        this.mIntent = mIntent;
        this.teamList = new ArrayList<>();
    }

    public List<Team> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.teamList = teamList;
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = mInflater.inflate(R.layout.item_team, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cardView.setBackgroundColor(Color.parseColor("#f5f5f5"));
        Team team = teamList.get(position);
        holder.tvTeamName.setText(team.getName());
        Glide.with(mContext).load(team.getUrlImage()).into(holder.ivTeam);

        holder.ivTeam.setOnClickListener(view -> {
            mIntent.putExtra("teamName", teamList.get(position).getName());
            mIntent.putExtra("imageUrl", teamList.get(position).getUrlImage());
            mIntent.putExtra("teamVictories", teamList.get(position).getVictories());
            mIntent.putExtra("teamLosses", teamList.get(position).getLosses());
            mIntent.setClass(mContext, TeamInfoActivity.class);
            mContext.startActivity(mIntent);
        });

        holder.iconPlusPlayer.setOnClickListener(view -> {
            mIntent.putExtra("position", position);
            if(teamList.get(position).getStringPlayerMap()!= null){
                mIntent.putExtra("teamPlayerMapSize", teamList.get(position).getStringPlayerMap().size());
            }
            mIntent.setClass(mContext, PlayersActivity.class);
            mContext.startActivity(mIntent);
        });

        holder.iconListPlayers.setOnClickListener(view -> {
            Toast.makeText(mContext, "Lista de jugadores del equipo aún no desarrollada", Toast.LENGTH_SHORT).show();
        });

        holder.iconShareTeam.setOnClickListener(view -> {
            Toast.makeText(mContext, "Compartir equipo en redes sociales aún no desarrollada", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
