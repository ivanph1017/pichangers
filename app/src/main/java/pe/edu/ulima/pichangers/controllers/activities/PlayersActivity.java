package pe.edu.ulima.pichangers.controllers.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.HashMap;
import java.util.Map;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.controllers.adapters.PlayersRecyclerAdapter;
import pe.edu.ulima.pichangers.models.dtos.Player;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseAuth;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseBD;

/**
 * Created by Ivan on 27/11/2016.
 */

public class PlayersActivity extends AppCompatActivity implements GestorFirebaseBD.OnComplete{
    private DatabaseReference databaseReference;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private PlayersRecyclerAdapter playersRecyclerAdapter;
    private GestorFirebaseBD gestorFirebaseBD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_players);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Escoja integrante");
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.arrow_left);
        actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        playersRecyclerAdapter = new PlayersRecyclerAdapter(this, getIntent());
        recyclerView.setAdapter(playersRecyclerAdapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("players");
        gestorFirebaseBD = new GestorFirebaseBD(this, databaseReference);
        gestorFirebaseBD.readData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            getIntent().setClass(this, TeamsActivity.class);
            startActivity(getIntent());
            return super.onOptionsItemSelected(item);
        }else if(id == R.id.logout){
            FirebaseAuth.getInstance().signOut();
            GestorFirebaseAuth gestorFirebaseAuth = new GestorFirebaseAuth();
            gestorFirebaseAuth.logout();
            getIntent().setClass(this, LoginActivity.class);
            startActivity(getIntent());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onError(String error) {
        Log.e("PlayersActivity", error);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReadComplete(DataSnapshot dataSnapshot) {
        Map<String, Player> stringPlayerMap = dataSnapshot.getValue(new GenericTypeIndicator<Map<String, Player>>(){
        });

        Map<String, Player> temp = new HashMap<>();
        temp.putAll(stringPlayerMap);
        for(Map.Entry<String, Player> bin : stringPlayerMap.entrySet()){
            if(bin.getValue() == null){
                stringPlayerMap.remove(bin);
            }
        }
        playersRecyclerAdapter.setStringPlayerMap(stringPlayerMap);
        playersRecyclerAdapter.notifyDataSetChanged();
    }
}
