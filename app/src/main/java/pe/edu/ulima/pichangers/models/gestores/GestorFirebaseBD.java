package pe.edu.ulima.pichangers.models.gestores;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Ivan on 27/11/2016.
 */

public class GestorFirebaseBD {
    private DatabaseReference mDatabase;
    private OnComplete callback;

    public GestorFirebaseBD(OnComplete callback, DatabaseReference mDatabase) {
        this.callback = callback;
        this.mDatabase = mDatabase;
    }

    public void readData() {
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.onReadComplete(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        };
        mDatabase.addValueEventListener(listener);

    }

    public void writeData(Object object){
        mDatabase.push().setValue(object);
    }

    public void removeData(){
        mDatabase.removeValue();
    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }

    public void setmDatabase(DatabaseReference mDatabase) {
        this.mDatabase = mDatabase;
    }

    public interface OnComplete {
        public void onReadComplete(DataSnapshot dataSnapshot);
        public void onError(String error);
    }
}


