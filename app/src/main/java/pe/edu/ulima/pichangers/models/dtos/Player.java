package pe.edu.ulima.pichangers.models.dtos;

/**
 * Created by Ivan on 27/11/2016.
 */

public class Player {
    private String nombre;
    private int codigo;

    public Player() {
    }

    public Player(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
