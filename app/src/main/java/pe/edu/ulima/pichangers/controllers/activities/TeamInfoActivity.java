package pe.edu.ulima.pichangers.controllers.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseAuth;

/**
 * Created by Ivan on 27/11/2016.
 */

public class TeamInfoActivity extends AppCompatActivity {
    private ImageView ivTeam;
    private TextView tvTeamName;
    private TextView tvVictories;
    private TextView tvLosses;
    private Bundle bundle;
    private String victories;
    private String losses;
    private Toolbar toolbar;
    private ActionBar actionBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_info);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Información");
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.arrow_left);
        actionBar.setDisplayHomeAsUpEnabled(true);

        ivTeam = (ImageView) findViewById(R.id.ivTeam);
        tvTeamName = (TextView) findViewById(R.id.tvTeamName);
        tvVictories = (TextView) findViewById(R.id.tvVictories);
        tvLosses = (TextView) findViewById(R.id.tvLosses);

        bundle = getIntent().getExtras();
        tvTeamName.setText(bundle.getString("teamName"));
        Glide.with(this).load(bundle.getString("imageUrl")).into(ivTeam);

        victories = tvVictories.getText().toString();
        losses = tvLosses.getText().toString();

        victories = String.format(victories, bundle.getInt("teamVictories"));
        losses = String.format(losses, bundle.getInt("teamLosses"));

        tvVictories.setText(victories);
        tvLosses.setText(losses);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            getIntent().setClass(this, TeamsActivity.class);
            startActivity(getIntent());
            return super.onOptionsItemSelected(item);
        }else if(id == R.id.logout){
            FirebaseAuth.getInstance().signOut();
            GestorFirebaseAuth gestorFirebaseAuth = new GestorFirebaseAuth();
            gestorFirebaseAuth.logout();
            getIntent().setClass(this, LoginActivity.class);
            startActivity(getIntent());
        }
        return super.onOptionsItemSelected(item);
    }
}
