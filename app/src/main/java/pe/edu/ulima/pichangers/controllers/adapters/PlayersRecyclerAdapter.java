package pe.edu.ulima.pichangers.controllers.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.controllers.activities.TeamsActivity;
import pe.edu.ulima.pichangers.models.dtos.Player;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseBD;

/**
 * Created by Ivan on 28/11/2016.
 */

public class PlayersRecyclerAdapter extends RecyclerView.Adapter<PlayersRecyclerAdapter.ViewHolder>{
    private List<Player> playerList;
    private List<String> timestampKeysList;
    private Map<String, Player> stringPlayerMap;
    private LayoutInflater mInflater;
    private Context mContext;
    private Intent mIntent;
    private DatabaseReference databaseReference;
    private GestorFirebaseBD gestorFirebaseBD;
    private int teamPlayerMapSize;

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView iconPlusPlayer;
        protected TextView tviNombre;
        protected TextView tviCodigo;
        protected PercentRelativeLayout layoutBody;

        public ViewHolder(View itemView) {
            super(itemView);
            iconPlusPlayer = (ImageView) itemView.findViewById(R.id.iconPlusPlayer);
            tviNombre = (TextView) itemView.findViewById(R.id.tviNombre);
            tviCodigo = (TextView) itemView.findViewById(R.id.tviCodigo);
            layoutBody = (PercentRelativeLayout) itemView.findViewById(R.id.layoutBody);
        }
    }

    public PlayersRecyclerAdapter(Context mContext, Intent mIntent) {
        this.mInflater = LayoutInflater.from(mContext);
        this.mContext = mContext;
        this.mIntent = mIntent;
        this.playerList = new ArrayList<>();
        this.timestampKeysList = new ArrayList<>();
        this.teamPlayerMapSize = mIntent.getExtras().getInt("teamPlayerMapSize", 0);
    }

    public Map<String, Player> getStringPlayerMap() {
        return stringPlayerMap;
    }

    public void setStringPlayerMap(Map<String, Player> stringPlayerMap) {
        this.stringPlayerMap = stringPlayerMap;
        this.timestampKeysList.clear();
        this.timestampKeysList.addAll(stringPlayerMap.keySet());
        this.playerList.clear();
        this.playerList.addAll(stringPlayerMap.values());
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = mInflater.inflate(R.layout.item_player, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(playerList.get(position) != null){
            switch (position % 3) {
                case 0: holder.layoutBody.setBackgroundColor(Color.parseColor("#669900")); break;
                case 1: holder.layoutBody.setBackgroundColor(Color.parseColor("#0091EA")); break;
                case 2: holder.layoutBody.setBackgroundColor(Color.parseColor("#FC6E04")); break;
            }
            holder.tviNombre.setText(playerList.get(position).getNombre());
            holder.tviCodigo.setText(String.valueOf(playerList.get(position).getCodigo()));
            holder.layoutBody.setOnClickListener(view -> {
                int idTeam = mIntent.getExtras().getInt("position");

                databaseReference = FirebaseDatabase.getInstance().getReference()
                        .child("teams").child(String.valueOf(idTeam)).child("playerList");
                gestorFirebaseBD = new GestorFirebaseBD((GestorFirebaseBD.OnComplete) mContext, databaseReference);
                gestorFirebaseBD.writeData(playerList.get(position));

                databaseReference = FirebaseDatabase.getInstance().getReference()
                        .child("players").child(timestampKeysList.get(position));
                gestorFirebaseBD.setmDatabase(databaseReference);
                gestorFirebaseBD.removeData();

                mIntent.setClass(mContext, TeamsActivity.class);
                mContext.startActivity(mIntent);
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
