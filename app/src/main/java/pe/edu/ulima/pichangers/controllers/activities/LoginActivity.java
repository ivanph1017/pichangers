package pe.edu.ulima.pichangers.controllers.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseAuth;

/**
 * Created by Ivan on 27/11/2016.
 */

public class LoginActivity extends AppCompatActivity implements GestorFirebaseAuth.OnComplete{
    private LoginButton loginButton;
    private ImageView ivFondo;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private GestorFirebaseAuth gestorFirebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        ivFondo = (ImageView) findViewById(R.id.fondo);
        Glide.with(this).load("https://i.imgsafe.org/b608194ac2.jpg").into(ivFondo);
        mAuth = FirebaseAuth.getInstance();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");

        mAuthListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                // User is signed in
                Log.d("LoginActivity", "onAuthStateChanged:signed_in:" + user.getUid());
            } else {
                // User is signed out
                Log.d("LoginActivity", "onAuthStateChanged:signed_out");
            }
        };

        gestorFirebaseAuth = new GestorFirebaseAuth(this, this);
        gestorFirebaseAuth.logout();
        gestorFirebaseAuth.authFacebook(loginButton);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        gestorFirebaseAuth.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onComplete(Task task) {
        Log.d("LoginActivity", "signInWithCredential:onComplete:" + task.isSuccessful());
        Intent intent = new Intent();
        intent.setClass(this, TeamsActivity.class);
        startActivity(intent);
        // If sign in fails, display a message to the user. If sign in succeeds
        // the auth state listener will be notified and logic to handle the
        // signed in user can be handled in the listener.
        if (!task.isSuccessful()) {
            Log.w("LoginActivity", "signInWithCredential", task.getException());
            Toast.makeText(this, "Authentication failed.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
