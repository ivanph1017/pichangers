package pe.edu.ulima.pichangers.controllers.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.List;

import pe.edu.ulima.pichangers.R;
import pe.edu.ulima.pichangers.controllers.adapters.TeamsRecyclerAdapter;
import pe.edu.ulima.pichangers.models.dtos.Team;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseAuth;
import pe.edu.ulima.pichangers.models.gestores.GestorFirebaseBD;
import pe.edu.ulima.pichangers.views.ItemOffsetDecoration;


/**
 * Created by Ivan on 27/11/2016.
 */

public class TeamsActivity extends AppCompatActivity implements GestorFirebaseBD.OnComplete{
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private TeamsRecyclerAdapter teamsRecyclerAdapter;
    private DatabaseReference databaseReference;
    private GestorFirebaseBD gestorFirebaseBD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_teams);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Equipos");
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.arrow_left);
        actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        teamsRecyclerAdapter = new TeamsRecyclerAdapter(this, getIntent());
        recyclerView.setAdapter(teamsRecyclerAdapter);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(itemAnimator);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("teams");
        gestorFirebaseBD = new GestorFirebaseBD(this, databaseReference);
        gestorFirebaseBD.readData();
    }

    @Override
    public void onError(String error) {
        Log.e("TeamsActivity", error);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReadComplete(DataSnapshot dataSnapshot) {
        List<Team> teamList = dataSnapshot.getValue(new GenericTypeIndicator<List<Team>>() {
        });
        teamsRecyclerAdapter.setTeamList(teamList);
        teamsRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FirebaseAuth.getInstance().signOut();
        GestorFirebaseAuth gestorFirebaseAuth = new GestorFirebaseAuth();
        gestorFirebaseAuth.logout();
        getIntent().setClass(this, LoginActivity.class);
        startActivity(getIntent());
        return super.onOptionsItemSelected(item);
    }
}
